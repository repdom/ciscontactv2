-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2019-03-01 06:28:51.719

-- tables
-- Table: contacto
CREATE TABLE contacto (
    codigo int NOT NULL AUTO_INCREMENT,
    nombre_contacto varchar(256) NOT NULL,
    telefono varchar(11) NOT NULL,
    cargo varchar(256) NULL,
    empresa_codigo int NOT NULL,
    nota varchar(4064) NOT NULL,
    CONSTRAINT contacto_pk PRIMARY KEY (codigo)
);

-- Table: empresa
CREATE TABLE empresa (
    codigo int NOT NULL AUTO_INCREMENT,
    nombre varchar(256) NOT NULL,
    CONSTRAINT empresa_pk PRIMARY KEY (codigo)
);

-- Table: historial
CREATE TABLE historial (
    codigo int NOT NULL AUTO_INCREMENT,
    fecha_llamada timestamp NOT NULL,
    usuario_llamada int NOT NULL,
    empresa_codigo int NOT NULL,
    CONSTRAINT historial_pk PRIMARY KEY (codigo)
);

-- foreign keys
-- Reference: contacto_empresa (table: contacto)
ALTER TABLE contacto ADD CONSTRAINT contacto_empresa FOREIGN KEY contacto_empresa (empresa_codigo)
    REFERENCES empresa (codigo);

-- Reference: historial_empresa (table: historial)
ALTER TABLE historial ADD CONSTRAINT historial_empresa FOREIGN KEY historial_empresa (empresa_codigo)
    REFERENCES empresa (codigo);

-- End of file.

